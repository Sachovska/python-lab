import shelve

db = shelve.open("file.txt")
clubs = [["FC Barcelona", "Spain", '1970', "Bartomeo", 122000, 30],
         ["FC Real Madrid", "Spain", '1960', "Peres", 122000, 29],
         ["FC Arsenal", "English", '1950', "Samir", 122200, 70]]
id = 1
for club in clubs:
    db[str(id)] = club
    id += 1

b = db["1"]
for club in db.values():
    if club[5] > b[5]:
        b = club

print("The most awarded club:", b)

country = "Spain"
clubsOfCountry = []
for club in db.values():
    if club[1] == country:
        clubsOfCountry.append(club)

sum = 0
for club in clubsOfCountry:
    sum += club[4]

print("Budget equal:", sum)
db.close()
