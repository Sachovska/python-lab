from tkinter import *


class Gui(object):
    def __init__(self, parent):
        self.top = parent

        self.lblFirst = Label(self.top, text="Enter first number:")
        self.lblFirst.grid(column=0, row=0)
        self.txtFirst = Entry(self.top, width=10)
        self.txtFirst.grid(column=1, row=0)

        self.lblSecond = Label(self.top, text="Enter second number:")
        self.lblSecond.grid(column=0, row=1)
        self.txtSecond = Entry(self.top, width=10)
        self.txtSecond.grid(column=1, row=1)

        self.checked1 = IntVar()
        self.checked2 = IntVar()

        self.c1 = Radiobutton(self.top, text='Devide', variable=self.checked1, value=1, command=self.deselectC2)
        self.c1.grid(column=1, row=2)
        self.c2 = Radiobutton(self.top, text='Multiply', variable=self.checked2, value=2, command=self.deselectC1)
        self.c2.grid(column=2, row=2)

        self.btn = Button(self.top, text="Calculate", command=self.callback)
        self.btn.grid(column=1, row=3)

        self.lblResult = Label(self.top, text="Result: ")
        self.lblResult.grid(column=2, row=0)

    def callback(self):
        if (self.checked1.get()):
            self.devide()
        elif (self.checked2.get()):
            self.mul()
        else:
            self.lblResult.configure(text="Choose operation")

    def devide(self):
        if(float(self.txtSecond.get()) == 0):
            self.lblResult.configure(text="Can't delete")
        elif(float(self.txtFirst.get()) == 0):
            self.lblResult.configure(text="0")
        else:
            res = float(self.txtFirst.get()) / float(self.txtSecond.get())
            self.lblResult.configure(text=res)

    def mul(self):
        res = float(self.txtFirst.get()) * float(self.txtSecond.get())
        self.lblResult.configure(text=res)

    def deselectC2(self):
        self.c2.deselect()

    def deselectC1(self):
        self.c1.deselect()


root = Tk()
root.title("App")
root.geometry('350x200')

my_window = Gui(root)

root.mainloop()