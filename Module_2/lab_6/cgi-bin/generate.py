#!/usr/bin/env python3
import cgi


class Devide():
    def __init__(self, lessNum, num):
        self.lessNum = lessNum
        self.num = num

    def devideNumbers(self):
        if float(self.lessNum) != 0:
            res = float(self.num) / float(self.lessNum)
        elif float(self.num) == 0:
            res = 0
        else:
            res = -1
        return res


form = cgi.FieldStorage()
firstNumber = form.getvalue("number_1", "0")
secondNumber = form.getvalue("number_2", "0")
result = -1
if float(firstNumber) > float(secondNumber):
    devide = Devide(secondNumber, firstNumber)
    res = devide.devideNumbers()
    result = "Can't devide" if (res == -1) else res
elif float(secondNumber) > float(firstNumber):
    devide = Devide(firstNumber, secondNumber)
    res = devide.devideNumbers()
    result = "Can't devide" if (res == -1) else res
elif secondNumber == firstNumber:
    result = float(secondNumber)/float(firstNumber)

print("Content-type: text/html\n")
print("""<!DOCTYPE HTML>
        <html>
        <head>
            <meta charset="utf-8">
            <title>Devide numbers</title>
        </head>
        <body>""")
print("<p>Result: {}</p>".format(result))

print("""</body>
        </html>""")
