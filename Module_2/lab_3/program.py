
length = int(input("Number of players: "))

players = {}
counter = []
players_list = []

for i in range(length):
    name = input("Player " + str(i + 1) + " | Name: ")
    score = input("Player " + str(i + 1) + " | Score: ")
    players[name] = score
    counter.append([name, i])
    players_list.append([name, score, i])

top = int(input("Input top lenght: "))
result = sorted(sorted(players_list, key=lambda x: x[2]), key=lambda x: x[1], reverse=True)

if len(result) < top:
    for i in range(len(result)):
        print(str(i + 1) + " | Player: " + result[i][0] + " | Score: " + result[i][1])
else:
    for i in range(top):
        print(str(i + 1) + " | Player: " + result[i][0] + " | Score: " + result[i][1])


