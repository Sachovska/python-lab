import math

def get_value_a(index):

    if index == 1: return 1
    
    return 0.5 * (math.sqrt(get_value_b(index - 1)) + 0.5 * math.sqrt(get_value_a(index - 1)))

def get_value_b(index):

    if index == 1: return 1
    
    return 2 * math.pow(get_value_a(index - 1), 2) + get_value_b(index - 1)

def factorial(n):
    if n == 0: return 1
    else: return n * factorial(n-1)

def calculate(n):
    return sum([(get_value_a(index) * get_value_b(index)) / factorial(index) for index in range(1, n + 1)])

print(calculate(5))