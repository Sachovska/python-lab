from random import randint
import sys

def get_matrix(matrix_height, matrix_width, is_random):
    
    if not(isinstance(matrix_height, int)):
        print("Matrix height argument is not integer!")
        return [[]]

    if not(isinstance(matrix_width, int)):
        print("Matrix width argument is not integer!")
        return [[]]

    if is_random:
        return create_random_matrix(matrix_height, matrix_width, 100)
    else:
        return input_matrix_values(matrix_height, matrix_width)

def create_random_matrix(matrix_height, matrix_width, max):

    Matrix = [[0 for x in range(matrix_width)] for y in range(matrix_height)]

    for line in range(len(Matrix)):
        for column in range(len(Matrix[line])):
            Matrix[line][column] = randint(0, max)

    return Matrix

def input_matrix_values(matrix_height, matrix_width):

    Matrix = [[0 for x in range(matrix_width)] for y in range(matrix_height)]

    for line in range(len(Matrix)):
        for column in range(len(Matrix[line])):
            Matrix[line][column] = int(input("Enter a Matrix[" + str(line) + "][" + str(column) + "]: "))

    return Matrix

def is_comes_down(one_dimension_array):

    for item in range(1, len(one_dimension_array)):
        if one_dimension_array[item] >= one_dimension_array[item - 1]:
            return False

    return True

def create_special_array(matrix):
    
    array = []

    for index in range(len(matrix)):
        if is_comes_down(matrix[index]):
            array.append(index)
    
    return array


print("Alive!")

m = get_matrix(3, 3, False)
print(m)
print(m[0])

for array in m:
    if is_comes_down(array):
        print("Comes down")
    else:
        print("Not")

print(create_special_array(m))
