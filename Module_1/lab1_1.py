import math
x=1
a=2
e=0.05

def double_fact(number):
    if number==0 or number==1:
        return 1
    else:
        return number*double_fact(number-2)

for k in range(1, 20):
    result = (math.sin(a ** k + x ** k))/double_fact(k)
    k+=e
    print(result)
