import person

class Provider():
	def __init__(self, students):
			self.students = students

	def getAge(year):
		return date.today().year - year
			
	def getAveragePointOfGroup(self, groupName):
		studentFromGroup = []
		suma=0
		for stud in self.students:
			if(stud.group == groupName):
				studentFromGroup.append(stud)
		
		countStudent = len(studentFromGroup)
		
		for stud in studentFromGroup:
			suma+=stud.studentShip
			
		return suma/countStudent
		
	def getAverageAgeOfGroup(self, groupName):
		personFromGroup = []
		suma=0
		for stud in self.students:
			if(stud.group == groupName):
				per = person.Person(stud.name, stud.surname, stud.day, stud.month, stud.year)
				personFromGroup.append(per)
		
		
		countStudent = len(personFromGroup)
		
		for per in personFromGroup:
			suma+= per.getAge()
			
		return suma/countStudent