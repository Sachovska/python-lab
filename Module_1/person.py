from datetime import date
class Person:
	def __init__(self, name, surname, day, month, year):
		self.name = name
		self.surname = surname
		self.day = day
		self.month = month
		self.year = year
			
	#method get age for person
	def getAge(self):
		return date.today().year - self.year
		
