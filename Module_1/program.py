import person
import student
import provider

#first
pers1 = person.Person("Vita", "Sachovska", 4, 7, 1998)
stud1 = student.Student(pers1, "Some", "401", 0, 83)

#second
pers2 = person.Person("Oleg", "Chornohirsky", 29, 9, 1997)
stud2 = student.Student(pers2, "Some", "401", 1200, 90)

#third
pers3 = person.Person("Vadim", "Voznuk", 10, 12, 1997)
stud3 = student.Student(pers3, "Some", "401", 0, 70)

#fourth
pers4 = person.Person("Karina", "Vasilkova", 9, 11, 1996)
stud4 = student.Student(pers4, " Some", "401", 894.56, 75)

studentGroup = [stud1, stud2, stud3, stud4]

pr = provider.Provider(studentGroup)
groupName = "401"
print(pr.getAveragePointOfGroup(groupName))
print(pr.getAverageAgeOfGroup(groupName))

