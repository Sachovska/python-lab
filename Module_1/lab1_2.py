import numpy as np

def removeElements(array):
    arrayChars = list(array)
    return ' '.join(char for char in arrayChars if char not in ' -?!,.;:{}')
  
firstString = "We , are young ?"
secondString = "You { are - young !"
firstArray = firstString.split()
secondArray = secondString.split()

comparedArray = np.intersect1d(firstArray, secondArray)
array = removeElements(comparedArray)
print(' '.join(comparedArray))
